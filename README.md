# DicasGit

## chave SSH
- ssh-keygen -t rsa -b 4096 -C "bruno@..." 
- enter
- cat  ~/.ssh/id_rsa.pub
- copiar codigo e colar no gitlab

## add user
- 1 - git config --global user.name "Bruno..."
- 2 - git config --global user.email "bruno@..." 

## cria o repositorio || entra no repositorio do projeto
- 3 - git clone git@gitlab.com:BrunoAnthonyShimura/teste1.git
- 4 - cd teste1        //entrar no diretorio "teste1.git"


## cria "README" se nao tiver
- 5 - touch README.md  
- 6 - git add README.md
- 7 - git commit -m "add README"
- 8 - git push -u origin master


## se quiser subir um arquivo 
- 1 - git status
- 2 - git add . 
- 3 - git commit -m "Projeto-2"
- 4 - git push -u origin master
